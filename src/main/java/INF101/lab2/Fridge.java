

package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	private int fridge_cap;
	private ArrayList<FridgeItem> fridge_inv;
	
	public Fridge() {
		this(20);
	}
	
	public Fridge(int max_cap) {
		this.fridge_cap = max_cap;
		this.fridge_inv = new ArrayList<FridgeItem>();
	}
	
	
	
	
	public int nItemsInFridge() {
		return this.fridge_inv.size();
	}
	
	
	public int totalSize() {
		return this.fridge_cap;
	}
	
	public boolean placeIn(FridgeItem item) {
		if (fridge_inv.size() < fridge_cap) {
			fridge_inv.add(item);
			return true;
		}
		else{
            return false;
        }
	}
	
	
	public void emptyFridge() {
		this.fridge_inv.clear();
		
	}

	public List<FridgeItem> removeExpiredFood() {
		ArrayList<FridgeItem> nExpired = new ArrayList<FridgeItem>();
		for (FridgeItem item : this.fridge_inv) {
			if (item.hasExpired()) {
				nExpired.add(item);
				
			}
		}
		this.fridge_inv.removeAll(nExpired);
		
		return nExpired;
	}
	
	public void takeOut(FridgeItem item) throws NoSuchElementException {
		if (this.fridge_inv.contains(item)) {
			this.fridge_inv.remove(item);
		}
		else {
            throw new NoSuchElementException("Item not in fridge");
        }
	
		
	}
	
	
	

}
